﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormLihatDataPegawaipns
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.DataPegawaiPNSToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TambahDataPegawaiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BiodataDiriToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DataJabatanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DataPangkatToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DataDiklatToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DataPendidikanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.KeluarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.Button5 = New System.Windows.Forms.Button()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(58, 254)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(862, 315)
        Me.DataGridView1.TabIndex = 0
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(283, 217)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "CARI"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(58, 217)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(198, 20)
        Me.TextBox1.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(55, 193)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(251, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "CARI DATA BERDASARKAN NIP/NAMA/BIDANG"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(363, 54)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(325, 20)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "DATA PEGAWAI NEGERI SIPIL (PNS) "
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(165, 88)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(664, 20)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "DINAS TENAGA KERJA DAN TRANSMIGRASI PROVINSI SUMATERA SELATAN"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(169, 616)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(110, 42)
        Me.Button2.TabIndex = 6
        Me.Button2.Text = "TAMBAH DATA "
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(450, 616)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(105, 43)
        Me.Button3.TabIndex = 6
        Me.Button3.Text = "EDIT DATA"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(602, 616)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(110, 43)
        Me.Button4.TabIndex = 6
        Me.Button4.Text = "HAPUS"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'MenuStrip1
        '
        Me.MenuStrip1.BackColor = System.Drawing.Color.ForestGreen
        Me.MenuStrip1.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DataPegawaiPNSToolStripMenuItem, Me.KeluarToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(983, 28)
        Me.MenuStrip1.TabIndex = 7
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'DataPegawaiPNSToolStripMenuItem
        '
        Me.DataPegawaiPNSToolStripMenuItem.DoubleClickEnabled = True
        Me.DataPegawaiPNSToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TambahDataPegawaiToolStripMenuItem})
        Me.DataPegawaiPNSToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.DataPegawaiPNSToolStripMenuItem.Name = "DataPegawaiPNSToolStripMenuItem"
        Me.DataPegawaiPNSToolStripMenuItem.Size = New System.Drawing.Size(149, 24)
        Me.DataPegawaiPNSToolStripMenuItem.Text = "&Data Pegawai PNS"
        '
        'TambahDataPegawaiToolStripMenuItem
        '
        Me.TambahDataPegawaiToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BiodataDiriToolStripMenuItem, Me.DataJabatanToolStripMenuItem, Me.DataPangkatToolStripMenuItem, Me.DataDiklatToolStripMenuItem, Me.DataPendidikanToolStripMenuItem})
        Me.TambahDataPegawaiToolStripMenuItem.Name = "TambahDataPegawaiToolStripMenuItem"
        Me.TambahDataPegawaiToolStripMenuItem.Size = New System.Drawing.Size(233, 24)
        Me.TambahDataPegawaiToolStripMenuItem.Text = "&Tambah Data Pegawai"
        '
        'BiodataDiriToolStripMenuItem
        '
        Me.BiodataDiriToolStripMenuItem.Name = "BiodataDiriToolStripMenuItem"
        Me.BiodataDiriToolStripMenuItem.Size = New System.Drawing.Size(192, 24)
        Me.BiodataDiriToolStripMenuItem.Text = "&Biodata Diri"
        '
        'DataJabatanToolStripMenuItem
        '
        Me.DataJabatanToolStripMenuItem.Name = "DataJabatanToolStripMenuItem"
        Me.DataJabatanToolStripMenuItem.Size = New System.Drawing.Size(192, 24)
        Me.DataJabatanToolStripMenuItem.Text = "&Data Jabatan"
        '
        'DataPangkatToolStripMenuItem
        '
        Me.DataPangkatToolStripMenuItem.Name = "DataPangkatToolStripMenuItem"
        Me.DataPangkatToolStripMenuItem.Size = New System.Drawing.Size(192, 24)
        Me.DataPangkatToolStripMenuItem.Text = "&Data Pangkat"
        '
        'DataDiklatToolStripMenuItem
        '
        Me.DataDiklatToolStripMenuItem.Name = "DataDiklatToolStripMenuItem"
        Me.DataDiklatToolStripMenuItem.Size = New System.Drawing.Size(192, 24)
        Me.DataDiklatToolStripMenuItem.Text = "&Data Diklat"
        '
        'DataPendidikanToolStripMenuItem
        '
        Me.DataPendidikanToolStripMenuItem.Name = "DataPendidikanToolStripMenuItem"
        Me.DataPendidikanToolStripMenuItem.Size = New System.Drawing.Size(192, 24)
        Me.DataPendidikanToolStripMenuItem.Text = "&Data Pendidikan"
        '
        'KeluarToolStripMenuItem
        '
        Me.KeluarToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.KeluarToolStripMenuItem.Name = "KeluarToolStripMenuItem"
        Me.KeluarToolStripMenuItem.Size = New System.Drawing.Size(195, 24)
        Me.KeluarToolStripMenuItem.Text = "&Kembali Ke Menu Utama"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.BackColor = System.Drawing.Color.ForestGreen
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 704)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.ManagerRenderMode
        Me.StatusStrip1.Size = New System.Drawing.Size(983, 22)
        Me.StatusStrip1.TabIndex = 8
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(320, 616)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(104, 43)
        Me.Button5.TabIndex = 17
        Me.Button5.Text = "LIHAT DATA"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'FormLihatDataPegawaipns
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(983, 726)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.DataGridView1)
        Me.Name = "FormLihatDataPegawaipns"
        Me.Text = "FormLihatDataPegawaipns"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents DataPegawaiPNSToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TambahDataPegawaiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BiodataDiriToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DataJabatanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DataPangkatToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DataDiklatToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DataPendidikanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents KeluarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents Button5 As System.Windows.Forms.Button
End Class
