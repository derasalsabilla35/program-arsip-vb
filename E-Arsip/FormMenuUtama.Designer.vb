﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormMenuUtama
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormMenuUtama))
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.DataPegawaiPNSToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TambahDataPegawaiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BiodataDiriToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DataJabatanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DataPangkatToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DataDiklatToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DataPendidikanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LihatDataPegawaiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DataPegawaiHonorerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TambahDataPegawaiToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.BiodataDiriToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.DataPendidikanToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.LihatDataPegawaiToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.DataPensiunPegawaiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TambahDataPensiunToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LihatDataPensiunToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.KeluarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.OdbcConnection1 = New System.Data.Odbc.OdbcConnection()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.MenuStrip1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.BackColor = System.Drawing.Color.ForestGreen
        Me.MenuStrip1.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DataPegawaiPNSToolStripMenuItem, Me.DataPegawaiHonorerToolStripMenuItem, Me.DataPensiunPegawaiToolStripMenuItem, Me.KeluarToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(1210, 28)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'DataPegawaiPNSToolStripMenuItem
        '
        Me.DataPegawaiPNSToolStripMenuItem.DoubleClickEnabled = True
        Me.DataPegawaiPNSToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TambahDataPegawaiToolStripMenuItem, Me.LihatDataPegawaiToolStripMenuItem})
        Me.DataPegawaiPNSToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.DataPegawaiPNSToolStripMenuItem.Name = "DataPegawaiPNSToolStripMenuItem"
        Me.DataPegawaiPNSToolStripMenuItem.Size = New System.Drawing.Size(149, 24)
        Me.DataPegawaiPNSToolStripMenuItem.Text = "&Data Pegawai PNS"
        '
        'TambahDataPegawaiToolStripMenuItem
        '
        Me.TambahDataPegawaiToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BiodataDiriToolStripMenuItem, Me.DataJabatanToolStripMenuItem, Me.DataPangkatToolStripMenuItem, Me.DataDiklatToolStripMenuItem, Me.DataPendidikanToolStripMenuItem})
        Me.TambahDataPegawaiToolStripMenuItem.Name = "TambahDataPegawaiToolStripMenuItem"
        Me.TambahDataPegawaiToolStripMenuItem.Size = New System.Drawing.Size(233, 24)
        Me.TambahDataPegawaiToolStripMenuItem.Text = "&Tambah Data Pegawai"
        '
        'BiodataDiriToolStripMenuItem
        '
        Me.BiodataDiriToolStripMenuItem.Name = "BiodataDiriToolStripMenuItem"
        Me.BiodataDiriToolStripMenuItem.Size = New System.Drawing.Size(192, 24)
        Me.BiodataDiriToolStripMenuItem.Text = "&Biodata Diri"
        '
        'DataJabatanToolStripMenuItem
        '
        Me.DataJabatanToolStripMenuItem.Name = "DataJabatanToolStripMenuItem"
        Me.DataJabatanToolStripMenuItem.Size = New System.Drawing.Size(192, 24)
        Me.DataJabatanToolStripMenuItem.Text = "&Data Jabatan"
        '
        'DataPangkatToolStripMenuItem
        '
        Me.DataPangkatToolStripMenuItem.Name = "DataPangkatToolStripMenuItem"
        Me.DataPangkatToolStripMenuItem.Size = New System.Drawing.Size(192, 24)
        Me.DataPangkatToolStripMenuItem.Text = "&Data Pangkat"
        '
        'DataDiklatToolStripMenuItem
        '
        Me.DataDiklatToolStripMenuItem.Name = "DataDiklatToolStripMenuItem"
        Me.DataDiklatToolStripMenuItem.Size = New System.Drawing.Size(192, 24)
        Me.DataDiklatToolStripMenuItem.Text = "&Data Diklat"
        '
        'DataPendidikanToolStripMenuItem
        '
        Me.DataPendidikanToolStripMenuItem.Name = "DataPendidikanToolStripMenuItem"
        Me.DataPendidikanToolStripMenuItem.Size = New System.Drawing.Size(192, 24)
        Me.DataPendidikanToolStripMenuItem.Text = "&Data Pendidikan"
        '
        'LihatDataPegawaiToolStripMenuItem
        '
        Me.LihatDataPegawaiToolStripMenuItem.Name = "LihatDataPegawaiToolStripMenuItem"
        Me.LihatDataPegawaiToolStripMenuItem.Size = New System.Drawing.Size(233, 24)
        Me.LihatDataPegawaiToolStripMenuItem.Text = "&Lihat Data Pegawai"
        '
        'DataPegawaiHonorerToolStripMenuItem
        '
        Me.DataPegawaiHonorerToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TambahDataPegawaiToolStripMenuItem1, Me.LihatDataPegawaiToolStripMenuItem1})
        Me.DataPegawaiHonorerToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.DataPegawaiHonorerToolStripMenuItem.Name = "DataPegawaiHonorerToolStripMenuItem"
        Me.DataPegawaiHonorerToolStripMenuItem.Size = New System.Drawing.Size(178, 24)
        Me.DataPegawaiHonorerToolStripMenuItem.Text = "&Data Pegawai Honorer"
        '
        'TambahDataPegawaiToolStripMenuItem1
        '
        Me.TambahDataPegawaiToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BiodataDiriToolStripMenuItem1, Me.DataPendidikanToolStripMenuItem1})
        Me.TambahDataPegawaiToolStripMenuItem1.Name = "TambahDataPegawaiToolStripMenuItem1"
        Me.TambahDataPegawaiToolStripMenuItem1.Size = New System.Drawing.Size(233, 24)
        Me.TambahDataPegawaiToolStripMenuItem1.Text = "&Tambah Data Pegawai"
        '
        'BiodataDiriToolStripMenuItem1
        '
        Me.BiodataDiriToolStripMenuItem1.Name = "BiodataDiriToolStripMenuItem1"
        Me.BiodataDiriToolStripMenuItem1.Size = New System.Drawing.Size(192, 24)
        Me.BiodataDiriToolStripMenuItem1.Text = "&Biodata Diri"
        '
        'DataPendidikanToolStripMenuItem1
        '
        Me.DataPendidikanToolStripMenuItem1.Name = "DataPendidikanToolStripMenuItem1"
        Me.DataPendidikanToolStripMenuItem1.Size = New System.Drawing.Size(192, 24)
        Me.DataPendidikanToolStripMenuItem1.Text = "&Data Pendidikan"
        '
        'LihatDataPegawaiToolStripMenuItem1
        '
        Me.LihatDataPegawaiToolStripMenuItem1.Name = "LihatDataPegawaiToolStripMenuItem1"
        Me.LihatDataPegawaiToolStripMenuItem1.Size = New System.Drawing.Size(233, 24)
        Me.LihatDataPegawaiToolStripMenuItem1.Text = "&Lihat Data Pegawai"
        '
        'DataPensiunPegawaiToolStripMenuItem
        '
        Me.DataPensiunPegawaiToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TambahDataPensiunToolStripMenuItem, Me.LihatDataPensiunToolStripMenuItem})
        Me.DataPensiunPegawaiToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.DataPensiunPegawaiToolStripMenuItem.Name = "DataPensiunPegawaiToolStripMenuItem"
        Me.DataPensiunPegawaiToolStripMenuItem.Size = New System.Drawing.Size(175, 24)
        Me.DataPensiunPegawaiToolStripMenuItem.Text = "&Data Pensiun Pegawai"
        '
        'TambahDataPensiunToolStripMenuItem
        '
        Me.TambahDataPensiunToolStripMenuItem.Name = "TambahDataPensiunToolStripMenuItem"
        Me.TambahDataPensiunToolStripMenuItem.Size = New System.Drawing.Size(230, 24)
        Me.TambahDataPensiunToolStripMenuItem.Text = "&Tambah Data Pensiun"
        '
        'LihatDataPensiunToolStripMenuItem
        '
        Me.LihatDataPensiunToolStripMenuItem.Name = "LihatDataPensiunToolStripMenuItem"
        Me.LihatDataPensiunToolStripMenuItem.Size = New System.Drawing.Size(230, 24)
        Me.LihatDataPensiunToolStripMenuItem.Text = "&Lihat Data Pensiun"
        '
        'KeluarToolStripMenuItem
        '
        Me.KeluarToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.KeluarToolStripMenuItem.Name = "KeluarToolStripMenuItem"
        Me.KeluarToolStripMenuItem.Size = New System.Drawing.Size(66, 24)
        Me.KeluarToolStripMenuItem.Text = "&Keluar"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.BackColor = System.Drawing.Color.ForestGreen
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 544)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.ManagerRenderMode
        Me.StatusStrip1.Size = New System.Drawing.Size(1210, 22)
        Me.StatusStrip1.TabIndex = 1
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Image = CType(resources.GetObject("Button1.Image"), System.Drawing.Image)
        Me.Button1.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button1.Location = New System.Drawing.Point(532, 240)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(253, 212)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = "&Data Pegawai Honorer"
        Me.Button1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(395, 75)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(604, 29)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "SISTEM INFORMASI ARSIP DATA KEPEGAWAIAN"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(367, 116)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(664, 20)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "DINAS TENAGA KERJA DAN TRANSMIGRASI PROVINSI SUMATERA SELATAN"
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Image = CType(resources.GetObject("Button2.Image"), System.Drawing.Image)
        Me.Button2.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button2.Location = New System.Drawing.Point(199, 240)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(240, 212)
        Me.Button2.TabIndex = 2
        Me.Button2.Text = "&Data Pegawai PNS"
        Me.Button2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button2.UseVisualStyleBackColor = True
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox1.Image = Global.E_Arsip.My.Resources.Resources.LOGO_PROVINSI_SUMATERA_SELATAN1
        Me.PictureBox1.Location = New System.Drawing.Point(289, 65)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(72, 71)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 5
        Me.PictureBox1.TabStop = False
        '
        'Button3
        '
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Image = CType(resources.GetObject("Button3.Image"), System.Drawing.Image)
        Me.Button3.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button3.Location = New System.Drawing.Point(863, 240)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(240, 212)
        Me.Button3.TabIndex = 2
        Me.Button3.Text = "&Data Pegawai Pensiun"
        Me.Button3.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button3.UseVisualStyleBackColor = True
        '
        'FormMenuUtama
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(1210, 566)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "FormMenuUtama"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Form Menu Utama"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents DataPegawaiPNSToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DataPegawaiHonorerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DataPensiunPegawaiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents KeluarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TambahDataPegawaiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LihatDataPegawaiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TambahDataPegawaiToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LihatDataPegawaiToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TambahDataPensiunToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LihatDataPensiunToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents OdbcConnection1 As System.Data.Odbc.OdbcConnection
    Friend WithEvents BiodataDiriToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DataJabatanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DataPangkatToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DataDiklatToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DataPendidikanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BiodataDiriToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DataPendidikanToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Button3 As System.Windows.Forms.Button

End Class
