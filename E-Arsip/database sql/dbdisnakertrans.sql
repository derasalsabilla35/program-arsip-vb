-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 02 Okt 2020 pada 19.33
-- Versi server: 10.1.38-MariaDB
-- Versi PHP: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbdisnakertrans`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbdiklat`
--

CREATE TABLE `tbdiklat` (
  `id_diklat` int(20) NOT NULL,
  `nama_diklat` text NOT NULL,
  `tempat_diklat` text NOT NULL,
  `tahun_diklat` year(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbdokumenpegawai`
--

CREATE TABLE `tbdokumenpegawai` (
  `id_dokumenpegawai` int(20) NOT NULL,
  `nip` int(20) NOT NULL,
  `nama_pegawai` varchar(100) NOT NULL,
  `nama_dokumen` text NOT NULL,
  `lokasi_dokumen` text NOT NULL,
  `dokumen_pegawai` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbjabatan`
--

CREATE TABLE `tbjabatan` (
  `id_jabatan` int(20) NOT NULL,
  `nip` int(20) NOT NULL,
  `nama_jabatan` text NOT NULL,
  `bidang` enum('Sekretariat Sub Bagian Umum Kepegawaian','Sekretariat Sub Bagian Keuangan','Sekretariat Sub Bagian Perencanaan, Evaluasi, dan Pelaporan / Program','Penempatan dan Perluasan Kerja','Pelatihan dan Produktivitas Tenaga Kerja','Hub Industrial, Syarat dan Jamsos','Pengawasan Ketenaga Kerjaan','Permukiman dan Penempatan Transmigrasi','PP, Usaha, Ekonomi, dan SOSBUD','UPTD - BLK PPKT','UPTD - HIPERKES') NOT NULL,
  `TMT_jabatan` date NOT NULL,
  `masa_kerja_tahun` int(5) NOT NULL,
  `masa_kerja_bulan` int(15) NOT NULL,
  `eselon` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbpangkat`
--

CREATE TABLE `tbpangkat` (
  `id_pangkat` int(20) NOT NULL,
  `nip` int(20) NOT NULL,
  `pangkat_pegawai` varchar(25) NOT NULL,
  `TMT_pangkat_pegawai` date NOT NULL,
  `golongan_pegawai` enum('I','II','III','IV') NOT NULL,
  `ruang_golongan_pegawai` enum('a','b','c','d','e') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbpegawaihonorer`
--

CREATE TABLE `tbpegawaihonorer` (
  `id_pegawaihonorer` int(20) NOT NULL,
  `nama_pegawai_honorer` varchar(100) NOT NULL,
  `tempat_lahir` varchar(50) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `alamat_pegawai_honorer` text NOT NULL,
  `agama_pegawai_honorer` enum('Islam','Kristen Katolik','Kristen Protestan','Hindu','Budha','Konghucu','Lainnya') NOT NULL,
  `jenis_kelamin` enum('Laki - Laki','Perempuan') NOT NULL,
  `status_perkawinan` enum('Kawin','Belum Kawin','Cerai Hidup','Cerai Mati') NOT NULL,
  `email` varchar(60) NOT NULL,
  `jabatan` text NOT NULL,
  `bidang_pegawai_honorer` enum('Sekretariat Sub Bagian Umum Kepegawaian','Sekretariat Sub Bagian Keuangan','Sekretariat Sub Bagian Perencanaan, Evaluasi, dan Pelaporan / Program','Penempatan dan Perluasan Kerja','Pelatihan dan Produktivitas Tenaga Kerja','Hub Industrial, Syarat dan Jamsos','Pengawasan Ketenaga Kerjaan','Permukiman dan Penempatan Transmigrasi','PP, Usaha, Ekonomi, dan SOSBUD','UPTD - BLK PPKT','UPTD - HIPERKES') NOT NULL,
  `foto_honorer` text NOT NULL,
  `id_pendidikanformal` int(20) NOT NULL,
  `id_dokumenpegawai` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbpegawaipns`
--

CREATE TABLE `tbpegawaipns` (
  `id_pegawaipns` int(20) NOT NULL,
  `nip` int(20) NOT NULL,
  `nama_pegawaipns` varchar(100) NOT NULL,
  `tempat_lahir_pegawai` varchar(100) NOT NULL,
  `tanggal_lahir_pegawai` date NOT NULL,
  `alamat_pegawai` text NOT NULL,
  `agama_pegawai` enum('Islam','Kristen Katolik','Kristen Protestan','Hindu','Budha','Konghucu','Lainnya') NOT NULL,
  `jenis_kelamin_pegawai` enum('Laki - Laki','Perempuan') NOT NULL,
  `status_perkawinan_pegawai` enum('Kawin','Belum Kawin','Cerai Hidup','Cerai Mati') NOT NULL,
  `masuk_cpns` date NOT NULL,
  `masuk_pns` date NOT NULL,
  `email` varchar(100) NOT NULL,
  `foto_pns` text NOT NULL,
  `id_jabatan` int(20) NOT NULL,
  `id_pangkat` int(20) NOT NULL,
  `id_pendidikanformal` int(20) NOT NULL,
  `id_diklat` int(20) NOT NULL,
  `id_dokumenpegawai` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbpendidikanformal`
--

CREATE TABLE `tbpendidikanformal` (
  `id_pendidikanformal` int(11) NOT NULL,
  `tingkatan` enum('SD','SMP','SMA/SMK','Diploma 1/D1','Diploma 2/D2','Diploma 3/D3','Diploma 4/D4','Sarjana/S1','Magister/S2','Doktor/S3') NOT NULL,
  `nama sekolah` varchar(60) NOT NULL,
  `jurusan` varchar(100) NOT NULL,
  `tahun_lulus` year(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbpensiun`
--

CREATE TABLE `tbpensiun` (
  `id_pensiun` int(20) NOT NULL,
  `nama_pegawai` varchar(100) NOT NULL,
  `nip` varchar(20) NOT NULL,
  `pangkat_pegawai` varchar(25) NOT NULL,
  `golongan_pegawai` enum('I','II','III','IV') NOT NULL,
  `ruang_golongan_pegawai` enum('a','b','c','d','e') NOT NULL,
  `jabatan_pegawai` text NOT NULL,
  `bidang_pegawai` enum('Sekretariat Sub Bagian Umum Kepegawaian','Sekretariat Sub Bagian Keuangan','Sekretariat Sub Bagian Perencanaan, Evaluasi, dan Pelaporan / Program','Penempatan dan Perluasan Kerja','Pelatihan dan Produktivitas Tenaga Kerja','Hub Industrial, Syarat dan Jamsos','Pengawasan Ketenaga Kerjaan','Permukiman dan Penempatan Transmigrasi','PP, Usaha, Ekonomi, dan SOSBUD','UPTD - BLK PPKT','UPTD - HIPERKES') NOT NULL,
  `TMT_pensiun` date NOT NULL,
  `keterangan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbusers`
--

CREATE TABLE `tbusers` (
  `id_user` int(11) NOT NULL,
  `nama_user` varchar(50) NOT NULL,
  `pw_user` varchar(30) NOT NULL,
  `level` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbusers`
--

INSERT INTO `tbusers` (`id_user`, `nama_user`, `pw_user`, `level`) VALUES
(1, 'disnakertrans', 'admin', 'admin'),
(2, 'user', '123456', 'user');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tbdiklat`
--
ALTER TABLE `tbdiklat`
  ADD PRIMARY KEY (`id_diklat`);

--
-- Indeks untuk tabel `tbdokumenpegawai`
--
ALTER TABLE `tbdokumenpegawai`
  ADD PRIMARY KEY (`id_dokumenpegawai`);

--
-- Indeks untuk tabel `tbjabatan`
--
ALTER TABLE `tbjabatan`
  ADD PRIMARY KEY (`id_jabatan`);

--
-- Indeks untuk tabel `tbpangkat`
--
ALTER TABLE `tbpangkat`
  ADD PRIMARY KEY (`id_pangkat`);

--
-- Indeks untuk tabel `tbpegawaipns`
--
ALTER TABLE `tbpegawaipns`
  ADD PRIMARY KEY (`nip`);

--
-- Indeks untuk tabel `tbpendidikanformal`
--
ALTER TABLE `tbpendidikanformal`
  ADD PRIMARY KEY (`id_pendidikanformal`);

--
-- Indeks untuk tabel `tbusers`
--
ALTER TABLE `tbusers`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tbdiklat`
--
ALTER TABLE `tbdiklat`
  MODIFY `id_diklat` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tbdokumenpegawai`
--
ALTER TABLE `tbdokumenpegawai`
  MODIFY `id_dokumenpegawai` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tbjabatan`
--
ALTER TABLE `tbjabatan`
  MODIFY `id_jabatan` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tbpangkat`
--
ALTER TABLE `tbpangkat`
  MODIFY `id_pangkat` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tbpendidikanformal`
--
ALTER TABLE `tbpendidikanformal`
  MODIFY `id_pendidikanformal` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tbusers`
--
ALTER TABLE `tbusers`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
